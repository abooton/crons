#!/usr/bin/env bash


# Ignore SIGHUP, SIGINT, SIGQUIT and SIGTERM signals to ensure that
# the script executes to completion.
trap '' 1 2 3 15


#
# Install the provided crontab to the specified user on the remote host. 
#
install_crontab(){
  local crontab="${1}"
  local user="${2}"
  local host="${3}"

  [ -n "${debug}" ] && echo "install \"${crontab}\" to ${user}@${host}"
  ssh ${user}@${host} "crontab -r" 2>/dev/null

  cat ${crontab} | ssh ${user}@${host} "crontab -"
  code=$?

  if [ ${code} -ne 0 ]
  then
    echo "${prog}: Failed to install \"${crontab}\" to ${user}@${host}."
    return 1
  fi

  return 0
}


prog=$(basename ${0} .sh)@${HOSTNAME}

# Enable debug diagnostics by defining any value for this variable.
debug=

[ ${#} -ne 1 ] && echo "Usage: ${prog} crontabs-directory" && exit 1
# Directory containing the crontabs.
crondir=${1}

# Check the crontabs directory exists - this is a hard-fault.
if [ ! -d ${crondir} ]
then
  echo "${prog}: Missing crontab directory \"${crondir}\"."
  exit 1
fi

# Get the registered list of crontabs.
crontabs=$(ls -1 ${crondir}/*.crontab*)

# Temporary working filename.
tmp_existing=${LOCALTEMP}/${prog}_$$_tmp_existing
tmp_target=${LOCALTEMP}/${prog}_$$_tmp_target

# Remove existing temporary files.
[ -f ${tmp_existing} ] && rm -f ${tmp_existing}
[ -f ${tmp_target} ] && rm -f ${tmp_target}

for crontab in ${crontabs}
do
  if [ "${crontab: -1}" == "~" ]
  then
    # Ignore temporary editor files.
    continue
  fi

  [ -n "${debug}" ] && (printf '=%.0s' {1..79}; echo)
  # Get the crontab filename.
  cfilename=$(basename ${crontab})
  [ -n "${debug}" ] && echo "crontab: ${cfilename}"

  # Parse the crontab filename for host and user.
  chost=$(echo ${cfilename} | cut -d'.' -f1)
  cuser=$(echo ${cfilename} | cut -d'.' -f2)
  [ -n "${debug}" ] && echo "user: ${chost}"
  [ -n "${debug}" ] && echo "host: ${cuser}"

  # Aquire the remote user crontab from the host
  ssh ${cuser}@${chost} "crontab -l" >${tmp_existing} 2>/dev/null
  code=$?

  # Generate a "processed" version of the crontab text..
  # .. first, add a header to warn about editing it.
  echo "# THIS CRONTAB IS MANAGED BY CRONANNY. ANY MODIFICATIONS " > ${tmp_target}
  echo "# MADE TO THIS CRONTAB DIRECTLY WILL BE DESTROYED THE NEXT TIME CRONANNY RUNS." >> ${tmp_target}
  echo "" >> ${tmp_target}
  # .. second, remove escaped line ends ("\\\n") to add readability, as cron does not allow them.
  cat ${crontab} | python2 -c "import sys; print sys.stdin.read().replace('\\\\\\n','');" >> ${tmp_target}

  # Check if something went wrong aquiring the remote user crontab.
  if [ ${code} -gt 1 ]
  then
    echo "${prog}: Failed to retrieve remote crontab for ${cuser}@${chost}."
    continue
  fi

  # Install if there is no remote user crontab detected.
  if [ ${code} -eq 1 ]
  then
    install_crontab ${tmp_target} ${cuser} ${chost}
    continue
  fi

  # Check that the installed remote user crontab is identical
  # to our expected crontab.
  expected=$(sha1sum ${tmp_target} | cut -d' ' -f1)
  [ -n "${debug}" ] && echo "expected: ${expected}"
  actual=$(sha1sum ${tmp_existing} | cut -d' ' -f1)
  [ -n "${debug}" ] && echo "actual  : ${actual}"

  if [ "${expected}" != "${actual}" ]
  then
    echo "${prog}: Installing new crontab for ${cuser}@${chost}."
    diff -u ${tmp_existing} ${tmp_target}
    install_crontab ${tmp_target} ${cuser} ${chost}
  fi

  [ -n "${debug}" ] && echo

done

# Clean up.
[ -f ${tmp_existing} ] && rm -f ${tmp_existing}
[ -f ${tmp_target} ] && rm -f ${tmp_target}
